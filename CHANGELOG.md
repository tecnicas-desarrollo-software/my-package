# CHANGELOG



## v0.2.2 (2024-04-07)

### Fix

* fix(deploy): Change the name of the folder from my-package to my_package ([`14fd18b`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/14fd18b88b947340c55a16778e147f9e68213e24))


## v0.2.1 (2024-04-07)

### Fix

* fix(deploy): add deploy pipeline ([`2c09a18`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/2c09a18ac16ba0743961e732836d8f5a962f25e4))

### Unknown

* create tests folder and drop the previous file there ([`0d83afd`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/0d83afd9f42639e41dfb7f35224b8c05297d55b8))

* Merge remote-tracking branch &#39;origin/main&#39; ([`7987691`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/7987691bb65b5a61710f3c3c37700130bcaa703c))

* create tests folder and add the previous test there ([`e097712`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/e097712d7b07077295969bd1e2b55bf39c871466))


## v0.2.0 (2024-04-07)

### Feature

* feat(test): add new test module ([`7293180`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/7293180beaa92591b32c126c6aaef44b2a5f93c4))


## v0.1.0 (2024-04-07)

### Feature

* feat(sr): add semantic release cd pipeline ([`47458c8`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/47458c86075b7600979aaebde31f3d905ad6504e))

### Unknown

* Add semantic_release config ([`d1a2166`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/d1a2166d20beef0c0b01ba6ad087669e6c2a7498))

* Add pyproject.toml to init poetry ([`a603de0`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/a603de0d097571f59950dd125ea12c7baf4f580f))

* Delete pyproject.toml ([`88c3d24`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/88c3d2415a77049407df64574c4aa9d281c062f8))

* Add new file ([`72a60e2`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/72a60e2bd8498e1c4741c5239f3d98789942c5e0))

* Update .gitlab-ci.yml file ([`4bba619`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/4bba61985c261a51b8defc6adc49481d6e65dba9))

* Add first pipeline ([`11d4f76`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/11d4f76cc0b177afe71c6fba0d4990e2e65f3635))

* Initial commit ([`65873ca`](https://gitlab.com/tecnicas-desarrollo-software/my-package/-/commit/65873ca51a29fd60bd1d9c3c37ba369d951f045d))
